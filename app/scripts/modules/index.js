const modules = {
  SkipLinks: ($el) => {
    import('./SkipLinks/SkipLinks.main').then((Module) => {
      new Module.default($el)
    }).catch((e) => console.error(e))
  },
  sampleModule: ($el) => {
    import('./sampleModule/sampleModule.main').then((Module) => {
      new Module.default($el)
    }).catch((e) => console.error(e))
  }
}

export default modules
