class SampleModule {
  constructor(el) {
    this.$el = el
    this.method(this.$el)
  }

  method($element) {
    console.log($element)
  }
}

export default SampleModule
