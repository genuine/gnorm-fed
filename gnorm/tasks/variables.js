import { src, dest } from 'gulp'
import { variables } from '../config'
import modify from 'gulp-modify'
import rename from 'gulp-rename'
import dartSass from 'sass'
import gulpSass from 'gulp-sass'
const sass = gulpSass(dartSass)

// Extracts JSON data from CSS comment
// https://github.com/oddbird/sassdoc-theme-herman/blob/master/sass-json-loader.js
function sassJsonLoader(source) {
  const startMarker = '/*! json-encode:'
  const endMarker = '*/'
  const start = source.indexOf(startMarker)
  const end = source.indexOf(endMarker, start)
  const jsondata = source.slice(start + startMarker.length, end)
  return jsondata
}

function buildVariables() {
  return src(variables.src)
    .pipe(
      sass({
        includePaths: ['./node_modules'],
      }).on('error', sass.logError)
    )
    .pipe(
      modify({
        fileModifier: function (file, contents) {
          return sassJsonLoader(contents.toString())
        },
      })
    )
    .pipe(
      rename({
        basename: '_styleguide',
        extname: '.json',
      })
    )
    .pipe(dest(variables.dest))
}

module.exports = {
  buildVariables: buildVariables,
}
